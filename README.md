# tinayiluo_IDS721_week5mini

[![pipeline status](https://gitlab.com/ly178/tinayiluo_ids721_week5mini/badges/main/pipeline.svg)](https://gitlab.com/ly178/tinayiluo_ids721_week5mini/-/commits/main)

## Purpose
This project is dedicated to developing a serverless application using AWS Lambda, programmed in Rust with Cargo Lambda. It integrates AWS Lambda with AWS DynamoDB to provide a "Quote of the Day" service. The application is designed to demonstrate the seamless integration of AWS Lambda, AWS API Gateway, and DynamoDB, showcasing serverless architecture's efficiency and scalability.

## Introduction
The core functionality of this project is a Lambda function that generates and delivers a random quote of the day to users. Users can trigger this function to receive a new quote, which is then stored in a DynamoDB table. This approach highlights the practical use of serverless computing for dynamic content generation and database interaction.

## Usability
Users can interact with the Lambda function by invoking it with `make aws-invoke` (appropriate credentials required) or through a provided API Gateway link. This makes the application accessible and easy to interact with for users seeking motivational quotes.

## Requirements
- **Development Environment**: Installation of Rust and Cargo Lambda is necessary, achievable through Homebrew commands: `brew install rust`, `brew tap cargo-lambda/cargo-lambda`, and `brew install cargo-lambda`.
- **AWS Account**: Users must have an AWS account to deploy and manage the Lambda function.

## Setup and Deployment
1. Initialize a new Cargo Lambda project: `cargo lambda new new-lambda-project`.
2. Incorporate required dependencies into `Cargo.toml`.
3. Implement quote generation logic within `src/main.rs`.
4. Local testing can be done using `cargo lambda watch`.
5. Invoke the Lambda function locally for testing with `cargo lambda invoke --data-ascii "{ \"command\": \"add\"}"`.
6. Utilize the AWS IAM console to create an IAM User for deployment.
7. Attach necessary policies (`lambdafullaccess` and `iamfullaccess`).
8. Generate access key in security credentials (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (us-east-1)).
9. Securely store the generated access key in a `.env` file, ensuring it's excluded from version control.
10. Facilitate deployment by exporting AWS credentials to Cargo Lambda. 

```bash
set -a # Automatically export all variables
source .env
set +a
```

11. Complete the build and deployment process using `cargo lambda build --release` and `cargo lambda deploy`.
12. Adjust Lambda function settings in AWS to permit DynamoDB access (`AWSDynamoDBFullAccess`).
13. Establish a DynamoDB table named "quote" with "quote_id" as the primary key.
14. Verify function effectiveness by ensuring DynamoDB reflects new entries.
15. Integrate the function with AWS API Gateway for HTTP access.
16. Finalize API deployment and note the invocation URL for user access.

### Integration with API Gateway
After deploying the API, navigate to `stages` within the AWS console to locate the invoke URL, such as:

```
https://oe1rvmzzl5.execute-api.us-east-1.amazonaws.com/quote/grab
```

#### Testing the API Gateway
Verify API functionality using Postman or via a curl request:

```bash
curl -X POST https://hh7sc6r1vl.execute-api.us-east-1.amazonaws.com/coin/flip \
  -H 'content-type: application/json' \
  -d '{ "command": "add"}'
```

#### Testing the Lambda Function Directly
For direct Lambda function testing, bypassing API Gateway:

```bash
cargo lambda invoke --remote tinayiluo_ids721_week5mini --data-ascii "{ \"command\": \"add\"}"
```

## Continuous Integration and Deployment
Ensure AWS credentials (`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`) are securely added to GitLab secrets for CI/CD automation. Utilize `.gitlab-ci.yml` and a `Makefile` for streamlined builds, tests, and deployments.

## Deliverables
This project successfully delivers an operational Lambda function, documented tests, a DynamoDB table setup for data storage, API Gateway integration for function invocation, and comprehensive test results via Postman.

### Lambda function 

![Screen_Shot_2024-02-23_at_8.58.41_PM](/uploads/3bc3167b0148c5a323a7b6565b740186/Screen_Shot_2024-02-23_at_8.58.41_PM.png)

### Lambda Test 

![Screen_Shot_2024-02-23_at_5.51.47_PM](/uploads/de9dbf41f279279fa54673902010c2ca/Screen_Shot_2024-02-23_at_5.51.47_PM.png)

### DynamoDB table

![Screen_Shot_2024-02-23_at_5.55.21_PM](/uploads/322a7e863aaf4607371d0e21040f4e60/Screen_Shot_2024-02-23_at_5.55.21_PM.png)

### API gateway

![Screen_Shot_2024-02-23_at_9.03.36_PM](/uploads/92af368c75c6f0752d4c001302f714d6/Screen_Shot_2024-02-23_at_9.03.36_PM.png)

### Postman Test

![Screen_Shot_2024-02-23_at_5.55.04_PM](/uploads/1e0ffad35304fe2fb83816147051bb55/Screen_Shot_2024-02-23_at_5.55.04_PM.png)